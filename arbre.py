import numpy
import pandas
import seaborn
import matplotlib.pyplot as plt

def main():
    battle = pandas.read_csv('battle.txt')

    #remplacer attacker_outcome par des binaires
    battle.attacker_outcome.replace('win',1,inplace=True)
    battle.attacker_outcome.replace('loss',0,inplace=True)

    battle.attacker_outcome.fillna(0,inplace=True)

    #visualisation de notre data
    seaborn.stripplot(x='attacker_1',y='attacker_size',hue='attacker_outcome', data=battle)
    #plt.show()

    #les données qui nous sommes utiles
    ss_outcome = battle[['attacker_size','defender_size','battle_type','major_death','major_capture','attacker_outcome']]

    ss_outcome.attacker_size.fillna('0',inplace=True)
    ss_outcome.defender_size.fillna('0',inplace=True)
    ss_outcome.battle_type.fillna('0',inplace=True)
    ss_outcome.major_death.fillna('0',inplace=True)
    ss_outcome.major_capture.fillna('0',inplace=True)

    #arbre de decision
    from sklearn.tree import DecisionTreeClassifier
    from sklearn.model_selection import train_test_split

    Tree = DecisionTreeClassifier()

    #pour la normalisation normalisation
    from sklearn import preprocessing
    pre = preprocessing.LabelEncoder()

    #ss_outcome.attacker_size = pre.fit_transform(ss_outcome.attacker_size)
    #ss_outcome.defender_size = pre.fit_transform(ss_outcome.defender_size)

    #normalisation des types de batailles
    ss_outcome.battle_type = pre.fit_transform(ss_outcome.battle_type)

    #separation de data X et target Y
    X = ss_outcome[['attacker_size','defender_size','battle_type','major_death','major_capture']]
    Y = ss_outcome['attacker_outcome']

    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, random_state=1)

    Tree.fit(X_train,Y_train)
    #Tree.predict([2,3])

    """from sklearn.model_selection import cross_val_score
    scores = cross_val_score(Tree,X,Y,cv=10,scoring='accuracy')
    scores.mean()*100"""

    print(X)
    print(Y)
    #result = Tree.predict(X)
    #print(scores)

    Y_predict = Tree.predict(X_test)
    print(Y_predict)

    from sklearn.metrics import accuracy_score

    score = accuracy_score(Y_test, Y_predict)
    print(score)
    #accuracy_score(result, Y)

    #affichage du graphe
    from sklearn.tree import export_graphviz
    # Export as dot file
    export_graphviz(Tree, out_file='tree.dot',
    rounded = True, proportion = False,
    precision = 2, filled = True)

    # Convert to png using system command (requires Graphviz)
    from subprocess import call
    call(['dot', '-Tpng', 'tree.dot', '-o', 'tree.png', '-Gdpi=600'])

    # Display in jupyter notebook
    from IPython.display import Image
    Image(filename = 'tree.png')

main()
