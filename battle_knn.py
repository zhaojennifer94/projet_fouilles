#!/usr/bin/env python3
import math
import random
from sklearn.model_selection import StratifiedKFold

def split_lines(input, seed, output1, output2):
    #fonction du TD
  random.seed(seed)
  out1 = open(output1, 'w')
  out2 = open(output2, 'w')
  for line in open(input, 'r').readlines():
      if line[:4] == 'name':
          continue
      else:
          if random.randint(0, 1):
              out1.write(line)
          else:
              out2.write(line)

'''Simplification des colonnes avec plusieurs données, on ne prend que la premiere donnée
Exemple: "Jaime Lannister, Andros Brax" dans la colonne attacker_commander'''
def read_data(filename):
    donnee =[]
    for line in open(filename, 'r').readlines():
        b = False
        sp = line.split(',')
        l = []
        for element in sp:
            if element[:1] == '\"':
                spl = element.split(',')
                l.append(spl[0][1:])
                b = True
            elif element[-1:] == '\"':
                b = False
                continue
            else:
                if b == False:
                    l.append(element.strip())
        donnee.append(l[:19])

    return donnee

"""Nettoyage des données et encodage de l'issu d'une battle
win = 1 , loss = 0
Colonnes pris en compte: (major_death, major_capture, attacker_size, defender_size)
Elimination des lignes avec des données manquantes"""
def clean_data(filename,donnee):
    donnee2 = []
    outcome = []
    attaqueurs = []
    defendeurs = []
    for i in range(len(donnee)):
        if donnee[i][15]=='' or donnee[i][16]=='' or donnee[i][17]=='' or donnee[i][18]=='':
            continue
        else:
            tmp = []
            tmp.append(donnee[i][15])
            tmp.append(donnee[i][16])
            tmp.append(donnee[i][17])
            tmp.append(donnee[i][18])
            donnee2.append(tmp)
            outcome.append((1 if donnee[i][13] == 'win' else 0))
            attaqueurs.append(donnee[i][5])
            defendeurs.append(donnee[i][9])
    return (donnee2,outcome,attaqueurs, defendeurs)


rd = read_data('train.txt')
rd1 = clean_data('train.txt', rd)
#print(rd1[3])


def simple_distance(data1, data2):
    return math.sqrt(sum(((data1[i]-data2[i])*(data1[i]-data2[i]) for i in range(len(data1)))))

"""
Retourne une paire (X, Y) de liste:
- X est une liste de N données: chaque donnée est une liste de 4 nombres
- Y est une liste de boolean : True à la position #i si outcome[i] est 1, sinon False
"""
def construct_data(donnee, outcome):
    X = []
    Y = []
    for i in range(len(outcome)):
        Y.append(outcome[i] == 1)
        X.append([float(x) for x in donnee[i]])
    return X, Y

rd3 = construct_data(rd1[0],rd1[1])
#print(rd3)

"""
Retourne une liste de taille k contenant les indices des elements les plus proches de x
"""
def k_nearest_neighbors(testInstance,donnee,k, dist_function):
    distances = []
    for i in range(len(donnee)):
        dist = dist_function(donnee[i], testInstance)
        distances.append((dist,i))
    distances.sort()
    return [ind[1] for ind in distances[:k]]

#rd4 = k_nearest_neighbors([0.0, 0.0, 6000.0, 12625.0],rd3[0], 2, simple_distance)
#print(rd4)

"""
Pour une instance donnée, dire si elle va gagner ou perdre
"""
def is_winner(testInstance, train_x, train_y, dist_function, k):
    knn = k_nearest_neighbors(testInstance, train_x, k, dist_function)
    numberT = 0
    numberF = 0
    for element in knn:
        if train_y[element]:
            numberT += 1
        elif train_y[element]:
            numberF += 1
    if numberT == numberF:
        return train_y[knn[0]]
    else:
        return numberT > numberF

#Pour vérifier [0.0, 0.0, 20000.0, 10000.0] -> false
#[0.0, 0.0, 6000.0, 12625.0] -> true
#rd5 = is_winner([0.0, 0.0, 20000.0, 10000.0], rd3[0], rd3[1], simple_distance, 2)
#print(rd5)

def eval_battle_classifier(train_x, train_y, test_x, test_y, classifier, dist_function, k):
    tab = []
    for i in range(len(test_x)):
        tab.append(classifier(test_x[i], train_x, train_y, dist_function,k))

    e = 0 #nombre erreurs
    for a in range(len(tab)):
        if(tab[a] != test_y[a]):
            e += 1
    return e/len(tab)

"""
Calcule la précision de notre classifieur
"""
def classifier_accuracy(train_x, train_y, test_x, test_y, classifier, dist_function, k):
    prediction = []
    for i in range(len(test_x)):
        prediction.append(classifier(test_x[i], train_x, train_y, dist_function,k))
    correct = 0
    for j in range(len(test_y)):
        if test_y[j] == prediction[j]:
            correct += 1
    return (correct/float(len(test_y))) * 100.0

read_test = read_data('test.txt')
rd1_test = clean_data('test.txt', read_test)
rd3_test = construct_data(rd1_test[0], rd1_test[1])
train_x, train_y = rd3[0], rd3[1]
test_x, test_y = rd3_test[0], rd3_test[1]

#acc = classifier_accuracy(train_x, train_y, test_x, test_y, is_winner ,simple_distance, 2)
#print(acc)
#rd6 = eval_battle_classifier(train_x, train_y, test_x, test_y, is_winner ,simple_distance, 2)
#print(rd6)

def sampled_range(mini, maxi, num):
  if not num:
    return []
  lmini = math.log(mini)
  lmaxi = math.log(maxi)
  ldelta = (lmaxi - lmini) / (num - 1)
  out = [x for x in set([int(math.exp(lmini + i * ldelta)) for i in range(num)])]
  out.sort()
  return out

def find_best_k(train_x, train_y, dist_function):
  num_splits = 2
  best_k = None
  lowest_error = float('inf')
  for k in sampled_range(1, len(train_x) // 2, 10):
    skf = StratifiedKFold(n_splits=num_splits, shuffle=True, random_state=0)
    error = 0
    for train_indices, test_indices in skf.split(train_x, train_y):
      subtrain_x = [train_x[i] for i in train_indices]
      subtrain_y = [train_y[i] for i in train_indices]
      subtest_x = [train_x[i] for i in test_indices]
      subtest_y = [train_y[i] for i in test_indices]
      error += eval_battle_classifier(subtrain_x, subtrain_y, subtest_x, subtest_y,
                                      is_winner, dist_function, k)
    error /= num_splits
    if error < lowest_error:
      best_k = k
      lowest_error = error
  print('Obtained error rate %f with K = %d' % (lowest_error, best_k))
  return best_k

#rd7 = find_best_k(train_x, train_y, simple_distance)
#print(rd7)
